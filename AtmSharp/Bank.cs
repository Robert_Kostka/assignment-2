﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;

namespace AtmSharp
{
    /// <summary>
    /// Represents a bank composed of a list of accounts.
    /// </summary>
    public class Bank
    {
        /// <summary>
        /// the list of accounts managed by the bank
        /// </summary>
        private List<Account> _accountList;

        /// <summary>
        /// The first account number. New account numbers are generated using an incremental process
        /// starting with this number. The "const" keyword makes it impossible to change the value
        /// of the field variable
        /// </summary>
        private const int DEFAULT_ACCT_NO_START = 100;

        /// <summary>
        /// The name of the folder containing all the account files. The "const" keyword makes it impossible to change the value
        /// of the field variable 
        /// </summary>
        private const string BANKING_DATA_FOLDER = "BankingData";

        /// <summary>
        /// Prefix used for account files storing regular account data. The "const" keyword makes it impossible to change the value
        /// of the field variable 
        /// </summary>
        private const string ACCT_FILE_PREFIX = "acct";

        /// <summary>
        /// Prefix used for account files storing checquing account data. The "const" keyword makes it impossible to change the value
        /// of the field variable
        /// </summary>
        private const string CHQ_ACCT_FILE_PREFIX = "chqacct";

        /// <summary>
        /// Prefix used for account files storing savings account data. The "const" keyword makes it impossible to change the value
        /// of the field variable
        /// </summary>
        private const string SAV_ACCT_FILE_PREFIX = "savacct";

        /// <summary>
        /// Constructor used to create bank objects. The bank will have a list of accounts which is to storage of acccounts
        /// the application works with
        /// </summary>
        public Bank()
        {
            _accountList = new List<Account>();
        }

        /// <summary>
        /// Load the account data for all the accounts. The account data files are stored in a directory
        /// named BankingData located in the current directory, the directory used to run the application from        
        /// </summary>
        public void LoadAccountData()
        {
            //get the directory path to the account files so they can be accessed through a dataDircetoryPath variable
            string dataDirectoryPath = Path.Combine(Directory.GetCurrentDirectory(), BANKING_DATA_FOLDER);

            //Check if the Directory exists
            if (Directory.Exists(dataDirectoryPath))
            {
                //Create a list for all of the account files in order to obtain the accounts
                string[] accountFileList = Directory.GetFiles(dataDirectoryPath);

                //use a foreach statement to go through every account file in the list, then next will be to load them
                foreach(string accountFile in accountFileList)
                {
                    //Use stream reader for all of the account files
                    using (StreamReader accountFileStreamRdr = new StreamReader(accountFile))
                    {
                        //Use a variable called accountNandT (account Name and Type) and read the contents
                        string accountNandT = accountFileStreamRdr.ReadLine();

                        Account account = null;

                        //Use a switch statement in order to go through the account names and types, between checquing and savings
                        switch(accountNandT)
                        {
                            ////////////////////////////////////////////////////////
                            //BASIC SWITCH STATEMENT FOR THE VARIOUS ACCOUNTS AND TYPES 
                            ////////////////////////////////////////////////////////
                            //Case statement for the Account in general
                            case "Account":
                                account = new Account();
                                break;
                            //case statement for a Checquing Account and its contents
                            case "ChecquingAccount":
                                account = new ChecquingAccount();
                                break;
                            //case statement for a Savings Account and its contents
                            case "SavingsAccount":
                                account = new SavingsAccount();
                                break;
                            //Use a default statement in case there is a situation in where there is an unkonwn account type 
                            default:
                                Debug.Assert(false, "This is an Unkown account type and will be diregarded.");
                                continue;
                        }
                        //Use this in order to load all of the information of the account
                        account.Load(accountFileStreamRdr);

                        //Use this to add the account into the account list for whatever purpose(retrieval, updating, etc.)
                        _accountList.Add(account);
                    }
                }
            }
            //get the list of files in the directory
            //go through the list of files, create the appropriate accounts and load the account files
            //if at this point the list of accounts is empty add the defaults accounts so the application is usable
            if(_accountList.Count == 0)
            {
                //Creates the default accounts
                CreateDefaultAccounts();
            }
        }

        /// <summary>
        /// Saves the data for all accounts in the data directory of the application. Each account is
        /// saved in a separate file which contains all the information and list of transactions performed
        /// in the account. The account data files are stored in a directory named BankingData located in the 
        /// current directory, the directory used to run the application from
        /// </summary>
        public void SaveAccountData()
        {
            //locate the path to the account files
            string dataDirectoryPath = Path.Combine(Directory.GetCurrentDirectory(), BANKING_DATA_FOLDER);

            //make the directory if it does not exist
            if (Directory.Exists(dataDirectoryPath) == false)
            {
                //if argument has been proven false, new directory has been created
                Directory.CreateDirectory(dataDirectoryPath);
            }

            //go through each account in the list of accounts and ask it to save itself into a corresponding file
            foreach (Account acct in _accountList)
            {
                //
                string prefix = (acct.GetType() == typeof(Account)) ? ACCT_FILE_PREFIX :
                    (acct.GetType() == typeof(ChecquingAccount)) ? CHQ_ACCT_FILE_PREFIX :
                    //Prefix used to store data of accounts
                    SAV_ACCT_FILE_PREFIX;

                //Had to change the mutator method to something that can actuallyallow the use of the Account Number
                string acctFileName = String.Format("{0}{1}.dat", prefix, acct.AccountNumber);
                ////////////////////////////////
                //
                
            }
        }

        /// <summary>
        /// Create 10 accounts with predefined IDs and balances. The default accounts are created only
        /// if no account data files exist
        /// </summary>
        private void CreateDefaultAccounts()
        {
            //repeat for as many default accounts need to be created
            for(byte iAccount = 0; iAccount < 10; iAccount++)
            {
                //create the account with required default properties
                //In this sense, use the '$' and put in iAccount when typing DefaultAccount from the Python app
                Account newDefAccount = new Account(DEFAULT_ACCT_NO_START + iAccount, $"DefaultAccount{iAccount}");
                //set the default deposit amount to 100
                newDefAccount.Deposit(100);
                //set the anuual interest rate to 2.5%
                newDefAccount.AnnualIntrRate = 2.5f;
                //add the account to the list
                _accountList.Add(newDefAccount);
            }

        }

        /// <summary>
        /// Create and store an account object with the required attributes
        /// </summary>
        /// <param name="clientName">the name of the account holder</param>
        /// <param name="acctType">the type of account to create</param>
        /// <returns>the account created</returns>
        public Account OpenAccount(string clientName, AccountType acctType)
        {
            //prompt the user for an account number
            int acctNo = DetermineAccountNumber();

            //create and store an account object with the required attributes
            Account newAccount = null;
            if (acctType == AccountType.Chequing)
            {
                newAccount = new ChecquingAccount(acctNo, clientName);
            }
            else if (acctType == AccountType.Savings)
            {
                newAccount = new SavingsAccount(acctNo, clientName);
            }

            //add the new account to the list of the accounts
            _accountList.Add(newAccount);

            //return the account to the caller so other properties can be set
            return newAccount;
        }

        /// <summary>
        /// Determine the account number prompting the user until they enter the correct information.
        /// The method throws an OperationCancel exception if the user chooses to terminate.
        /// </summary>
        /// <returns></returns>
        private int DetermineAccountNumber()
        {
            //repeat trying to ask the user for the required input until the input is correct or the user cancels
            while (true)
            {
                //ask the user for input
                Console.WriteLine("'Please enter the account number [100 - 1000] or press [ENTER] to cancel: ");
                string acctNoInput = Console.ReadLine();

                //check whether the user cancelled the operation
                if (acctNoInput.Length == 0)
                {
                    throw new OperationCanceledException("User has selected to termninate the program after invalid input.");
                }

                //check the input to ensure correctness and deal with incorrect input
                int acctNo = int.Parse(acctNoInput);
                if (acctNo < 100 || acctNo > 1000)
                {
                    throw new InvalidValueException("The account number you have entered is not valid. Please enter avalidaccount number.");
                }
                //check that the account number is not in use
                foreach (Account account in _accountList)
                {
                    if(acctNo == account.AccountNumber)
                    {
                        throw new InvalidValueException("the account number you have entered already exists. Please enter a valid account number.");
                    }
                }
                //the account number has been generated successfully
                return acctNo;
            }
        }

        /// <summary>
        /// Returns the account with the given account number or null if no account with that ID can be found
        /// </summary>
        /// <param name="acctNo">the account number of the account to return</param>
        /// <returns>the account object with the given ID</returns>
        public Account FindAccount(int acctNo)
        {
            //go through all the accounts until one is found with the given account number
            foreach (Account acct in _accountList)
            {
                if (acct.AccountNumber == acctNo)
                {
                    return acct;
                }
            }
            //if the program got here it means there was no account with the given account number
            return null;
        }


    }
}

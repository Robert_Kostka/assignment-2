﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace AtmSharp
{
    /// <summary>
    /// Valid user choices for the main menu
    /// </summary>
    enum MainMenuOption
    {
        SelectAccount = 1,
        CreateAccount,
        ExitAtmApplication
    }

    /// <summary>
    /// Valid user choices for the account menu
    /// </summary>
    enum AccountMenuOption
    {
        CheckBalance = 1,
        Withdraw,
        Deposit,
        DisplayTransactions,
        ExitAccount
    }

    /// <summary>
    /// The Atm class representing an ATM machine. The class displays and performs the the account management functions
    /// on a given bank account: checking balance, withdrawing and depositing money
    /// </summary>
    /// <remarks>
    /// Author: Magdin Stoica
    /// Version 3.2 (C#)    
    /// </remarks>
    public class Atm
    {
        /// <summary>
        /// the bank this ATM object is working with
        /// </summary>
        private Bank _bank;

        /// <summary>
        /// Atm constructor that initializes the bank the ATM works with
        /// </summary>
        /// <param name="bank">the bank that manages the account objects the ATM works with</param>
        public Atm(Bank bank)
        {
            _bank = bank;
        }

        /// <summary>
        /// Starts the ATM program by displaying the required user options. 
        /// User navigates the menus managing their accounts
        /// </summary>
        public void Start()
        {
            do
            {
                //The main menu is displayed and presents user with available choices
                MainMenuOption optionChosen = ShowMainMenu();

                //Create switch statement to display and allow user to interact with the menu, and depending on option
                //The method is run according to the selection made 
                switch (optionChosen)
                {
                    //Case for CreateAccountoption in case it is selected
                    case MainMenuOption.CreateAccount:
                        OnCreateAccount();
                        break;
                    //case for the SelectAccount method to be run in case this choice was selected
                    case MainMenuOption.SelectAccount:
                        Account acct = OnSelectAccount();
                        if (acct != null)
                        {
                            OnManageAccount(acct);
                        }
                        break;
                    //case for the application to exit if the user is finished using the app
                    case MainMenuOption.ExitAtmApplication:
                        OnExit();
                        return;
                    //Default case in case a different option is chosen than what was available and deal with it accordingly by presenting an error
                    default:
                        Console.WriteLine("Please choose one of the available options presented.");
                        break;

                }
            }
            //While loop for the main menu so that it repeats the user options until the user wishes to exit the application 
            while (true);
        }

        /// <summary>
        /// Displays the main ATM menu and ensure the user picks an option. Handles invalid input but doesn't check
        /// that the menu option is one of the displayed ones.
        /// </summary>
        /// <returns>the option selected by the user</returns>
        private MainMenuOption ShowMainMenu()
        {
            //Display menu and obtain selection from user. Return selected option.
            do
            {
                try
                {
                    //Present list of options
                    Console.WriteLine("\nMain Menu\n\n1: Select Account\n2: Create Account\n3: Exit\n\nEnter a choice: ");
                    
                    //Read Line to get the user input
                    return (MainMenuOption)int.Parse(Console.ReadLine());
                }
                //catch for an error in case user doesnt enter a valid menu option
                catch (FormatException)
                {
                    Console.WriteLine("Please Select one of the available options.", "\n");
                }
            }
            while (true);

        }

        /// <summary>
        /// Displays the ACCOUNT menu that allows the user to perform account operations. Handles invalid input but doesn't check
        /// that the menu option is one of the displayed ones.
        /// </summary>
        /// <returns>the option selected by the user</returns>
        private AccountMenuOption ShowAccountMenu()
        {
            //Display menu and obtain selection from user. Return selected option.
            ///////////////////////////////////////////////////////////
            //Similar to MainMenuOption method
            //////////////////////////////////////////////////////////
            do
            {
                try
                {
                    //Present possible menu options to the user
                    Console.WriteLine("\nAccount Menu\n\n1: Check Balance\n2: Withdraw\n3: Deposit\n4: Exit\n\nEnter a choice: ");

                    //return the input from the user
                    return (AccountMenuOption)int.Parse(Console.ReadLine());
                }
                //Catch statement in case there is an exception error
                catch (FormatException)
                {
                    //provide error in case user entered incorrect input
                    Console.WriteLine("Enter a Valid option.", "\n");
                }
            }
            while (true);
            //Unreachablecode detected
            //return AccountMenuOption.ExitAccount;
        }

        /// <summary>
        /// Create and open an account. The user is prompted for all account information including the type of account to open.
        /// Create the account object and add it to the bank
        /// </summary>
        private void OnCreateAccount()
        {
            //repeat trying to create an account until the user is successful or gives up
            while (true)
            {
                //get the name of the account holder from the user
                string clientName = PromptForClientName();

                //get the initial deposit from the user
                double initDepositAmount = PromptForDepositAmount();

                //get the annual interest rate from the user
                float annIntrRate = PromptForAnnualIntrRate();

                //get the account type from the user
                AccountType acctType = PromptForAccountType();

                //open the account
                Account newAccount = _bank.OpenAccount(clientName, acctType);
                //set the other account properties
                newAccount.Deposit(initDepositAmount);
                newAccount.SetAnnualIntrRate(annIntrRate);

                return;

                ////////////////////////////////////////
                //UNSURE HOW TO DO EXCEPTIONS ON THIS PART SO IM ASSUMING THE USER WOULD KNOW WHAT TO DO
                ///////////////////////////////////////////

                
            }
        }

        /// <summary>
        /// Select an account by prompting the user for an account number and remembering which account was selected.
        /// Prompt the user for performing account information such deposit and withdrawals
        /// </summary>
        /// <returns>
        ///     - the account object being selected or 
        ///     - null if the user cancelled; NOTE that this definition requires the caller to always use an if statement to check
        /// </returns>
        private Account OnSelectAccount()
        {
            //Attempt the user interaction  until all user information is provided correctly or the user cancels
            while (true)
            {
                //Prompt the user for the account number to select
                Console.WriteLine("Please enter your account ID or press [Enter] to cancel: ");
                string acctNoInput = Console.ReadLine();
                //check to see if the user gave up and is cancelling the operation                
                if (acctNoInput.Length == 0)
                {
                    return null;
                }

                //the user entered an account number get the actual number
                int acctNo =  int.Parse(acctNoInput);
                //obtain the account required by the user from the bank
                Account acct = _bank.FindAccount(acctNo);
                if (acct != null)
                {
                    return acct;
                }
                else
                {
                    Console.WriteLine("The account was not found. please select another account.");
                }
                return null;
            }
        }

        /// <summary>
        /// Manage the account by allowing the user to execute operation on the given account
        /// </summary>
        /// <param name="acct">the account to be managed</param>
        private void OnManageAccount(Account account)
        {
            //Attempt the user interaction  until all user information is provided correctly or the user cancels
            do
            {
                //display menu and obtain a user selection
                AccountMenuOption selAcctMenuOpt = ShowAccountMenu();

                //handle the user selection with the appropriate event handler
                //in this case, since there aremultiple options and choices possible, use switch statement to deal with
                //choices appropriately
                switch (selAcctMenuOpt)
                {
                    //case for the choice of wanting to check balance
                    case AccountMenuOption.CheckBalance:
                        OnCheckBalance(account);
                        break;

                    //case for the withdraw choice
                    case AccountMenuOption.Withdraw:
                        OnWithdraw(account);
                        break;

                    //Case for the deposit choice
                    case AccountMenuOption.Deposit:
                        OnDeposit(account);
                        break;

                    //case for the exit account choice
                    case AccountMenuOption.ExitAccount:
                        OnExit();
                        break;

                    //Default in case in case a valid menu option hasnt been chosen
                    default:
                        Console.WriteLine("Please enter a valid menu option");
                        break;


                }
            }
            while (true);
        }

        /// <summary>
        /// Prompts the user to enter the name of the client and allows the user to cancel by pressing ENTER
        /// </summary>
        /// <returns>the name the client creating the account</returns>
        private string PromptForClientName()
        {
            //Prompt for teh client name
            Console.WriteLine("Please enter the client name or press [Enter] to cancel the current operation.");

            string clientName = Console.ReadLine();
            //check whether the user input was valid
            if (clientName.Length == 0)
            {
                throw new OperationCanceledException("The user has selected to cancel the curent operation.");
            }

            return clientName;
        }

        /// <summary>
        /// Prompts the user to enter an account balance and performs basic error checking
        /// </summary>
        /// <returns>the amount to be deposited</returns>
        private double PromptForDepositAmount()
        {
            //repeat trying to ask the user for the required input until the input is correct or the user cancels
            while (true)
            {
                //Prompt for the initial balance
                Console.WriteLine("Please enter the initial amount balance.");

                //Read the initial amount balance entered by user
                float initAmount = float.Parse(Console.ReadLine());

                //check the user input
                if (initAmount >= 0)
                {
                    return initAmount;
                }
                else
                {
                    Console.WriteLine("Cannot create an account with a negative balance. Please enter a valid amount.");
                }

                //if we got to this point the amount is valid
                //^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
            }
        }

        /// <summary>
        /// Prompts the user to enter the annual interest rate for an account
        /// </summary>
        /// <returns>the annual interest rate for the account being created</returns>
        private float PromptForAnnualIntrRate()
        {
            //repeat trying to ask the user for the required input until the input is correct or the user cancels
            while (true)
            {
                //obtain the input from the user
                Console.WriteLine("Please enter the interest rate for this account: ");

                //ReadLine(); method to obtain the interest rate
                float intrRate = float.Parse(Console.ReadLine());
                //perform basic sanity checking of the input. Note that the business rules for checking are implemented
                //in the account classes not here so that they are together with the rest of the account business logic
                if(intrRate >= 0)
                {
                    //If interest rate is greater than 0, return the value
                    return intrRate;
                }
                //If value is less than 0, then present an error
                else
                {
                    Console.WriteLine("Cannot create an account with a negative interest rate.");
                }

                //if we got to this point the amount is valid
            }
        }

        /// <summary>
        /// Prompts the user to enter an account type
        /// </summary>
        /// <returns>the account type as a constant as an enum value</returns>
        private AccountType PromptForAccountType()
        {
            //repeat trying to ask the user for the required input until the input is correct or the user cancels
            while (true)
            {
                //Prompt the user for the account type
                Console.WriteLine("Please enter the account type [c/s: chequing / savings]: ");

                //read line in order to obtain the desiredaccount type
                string acctTypeInput = Console.ReadLine();

                //determine the account type based on the user input
                if (acctTypeInput == "c")
                {
                    return AccountType.Chequing;
                }
                else if (acctTypeInput == "s")
                {
                    return AccountType.Savings;
                }
                //error in case the value input as an answer is not "c" or "s" for savings and chequing respectively
                else
                {
                    Console.WriteLine("Answer not supported. Please enter one of the supported answers.");
                }
                
            }
        }

        /// <summary>
        /// Prints the balance in the given account
        /// </summary>
        /// <param name="account">the account for which the balance is printed </param>
        private void OnCheckBalance(Account account)
        {
            //Display the current balance of the account
            Console.WriteLine("The balance is {0}", account.GetBalance());
        }

        /// <summary>
        /// Prompts the user for an amount and performs the deposit. Handles any errors related to incorrect amounts
        /// </summary>
        /// <param name="account">the account in which the amount is to be deposited</param>
        private void OnDeposit(Account account)
        {
            //repeat trying to ask the user for the required input until the input is correct or the user cancels
            while (true)
            {
                //obtain the input amount from the user
                Console.WriteLine("Please enter an amount to deposit or press [Enter]to exit: ");

                string inputAmount = Console.ReadLine();

                //test for empty input in case the user pressed [ENTER] because they wanted to give up on depositing money
                if (inputAmount.Length > 0)
                {
                    float amount = float.Parse(inputAmount);

                    //deposit the amount into the desired account
                    account.Deposit(amount);
                }
                /////////////////else if(inputAmount == "Enter")

                //the deposit was done or user entered nothing so break from the infinite loop
                return;
            }
        }

        /// <summary>
        /// Prompts the user for an amount and performs the withdrawal. Handles any errors related to incorrect amounts
        /// </summary>
        /// <param name="account">the account in which the amount is to be withdrawn</param>
        private void OnWithdraw(Account account)
        {
            //repeat trying to ask the user for the required input until the input is correct or the user cancels
            while (true)
            {
                //obtain the input amount from the user
                Console.WriteLine("Please enter an amount to withdraw or type [Enter] to exit: ");
                string inputAmount = Console.ReadLine();

                //test for empty input in case the user pressed [ENTER] because they wanted to give up on withdrawing money
                if (inputAmount.Length > 0)
                {
                    float amount = float.Parse(inputAmount);
                    //Withdraw the selectedamount
                    account.Withdraw(amount);
                }
                //the withdrawal was done or user entered nothing so break from the infinite loop
                return;
            }
        }

        /// <summary>
        /// Event handler called when the user selects to exit the application. Ensures that all account data is saved.
        /// </summary>
        private void OnExit()
        {
            //the application is shutting down, save all account information
            _bank.SaveAccountData();
        }

    }
}
